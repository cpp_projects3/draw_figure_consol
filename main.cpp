#include <iostream>
#include <cmath>

#include <sys/ioctl.h>
#include <unistd.h>
#include "geometry.h"

int main() {
    struct winsize size_csl;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &size_csl);

    int width = size_csl.ws_col;
    int height = size_csl.ws_row;

    geomdraw::circle temp(width, height);
    for (int i = 0; i < 1000; ++i) {
        temp.DrawFigure(i);
    }

    geomdraw::d2 buff(1, 2);
    std::cout << buff.GetX() << std::endl;
    std::cout << buff.GetY() << std::endl;
}