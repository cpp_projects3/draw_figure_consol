#include "dimension3.h"

geomdraw::d3::d3(float value) : d2(value), z_(value) {}
geomdraw::d3::d3(float x, float y, float z) : d2(x, y), z_(z) {}
geomdraw::d3::d3(float x, const d2& v) : z_(v.GetY()), d2(x, v.GetX()) {}

float geomdraw::d3::GetZ() const noexcept {
    return z_;
}

void geomdraw::d3::SumD3(const d3& other) {
    d2::SumD2(other);
    z_ += other.z_;
}

void geomdraw::d3::SubD3(const d3& other) {
    d2::SubD2(other);
    z_ -= other.z_;
}

void geomdraw::d3::MulD3(const d3& other) {
    d2::MulD2(other);
    z_ *= other.z_;
}

void geomdraw::d3::DivD3(const d3& other) {
    d2::DivD2(other);
    if (other.z_ == 0) {
        throw std::logic_error("Can't divided by zero!");
    }
    z_ /= other.z_;
}

geomdraw::d3 geomdraw::d3::operator+(const geomdraw::d3& other) const {
    d3 result(*this);
    result.SumD3(other);
    return result;
}
geomdraw::d3 geomdraw::d3::operator-(const geomdraw::d3& other) const {
    d3 result(*this);
    result.SubD3(other);
    return result;
}
geomdraw::d3 geomdraw::d3::operator*(const geomdraw::d3& other) const {
    d3 result(*this);
    result.MulD3(other);
    return result;
}
geomdraw::d3 geomdraw::d3::operator/(const geomdraw::d3& other) const {
    d3 result(*this);
    result.DivD3(other);
    return result;
}
geomdraw::d3 geomdraw::d3::operator-() const {
    return d3(-x_, -y_, -z_);
}