#ifndef GEOMETRY_DIMENSION3_H
#define GEOMETRY_DIMENSION3_H

#include "dimension2.h"

namespace geomdraw {
class d3: public d2 {
protected:
    float z_;
public:
    explicit d3(float value);
    d3(float _x, const d2& v);
    d3(float _x, float _y, float _z);

    [[nodiscard]] float GetZ() const noexcept;
    void SumD3(const d3& other);
    void SubD3(const d3& other);
    void MulD3(const d3& other);
    void DivD3(const d3& other);

    d3 operator+(const d3& other) const;
    d3 operator-(const d3& other) const;
    d3 operator*(const d3& other) const;
    d3 operator/(const d3& other) const;
    d3 operator-() const;
};
} // namespace geomdraw
#endif //GEOMETRY_DIMENSION3_H
