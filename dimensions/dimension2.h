#ifndef GEOMETRY_DIMENSION2_H
#define GEOMETRY_DIMENSION2_H

#include <iostream>


namespace geomdraw {
class d2 {
protected:
    float x_, y_;
public:
    explicit d2(float value) noexcept;
    d2(float x, float y) noexcept;
    d2(const d2& other) noexcept;
    ~d2() = default;

    [[nodiscard]] float GetX() const noexcept;
    [[nodiscard]] float GetY() const noexcept;

    void SumD2(const d2& other);
    void SubD2(const d2& other);
    void MulD2(const d2& other);
    void DivD2(const d2& other);

    d2 operator+(const d2& other) const;
    d2 operator-(const d2& other) const;
    d2 operator*(const d2& other) const;
    d2 operator/(const d2& other) const;
};
} // namespace geomdraw
#endif //GEOMETRY_DIMENSION2_H
