#include "dimension2.h"

geomdraw::d2::d2(float value) noexcept : x_(value), y_(value) {}
geomdraw::d2::d2(float x, float y) noexcept : x_(x), y_(y) {}
geomdraw::d2::d2(const d2& other) noexcept : x_(other.x_), y_(other.y_) {}

float geomdraw::d2::GetX() const noexcept {
    return x_;
}
float geomdraw::d2::GetY() const noexcept {
    return y_;
}
void geomdraw::d2::SumD2(const d2& other) {
    x_ += other.x_;
    y_ += other.y_;
}
void geomdraw::d2::SubD2(const d2& other) {
    x_ -= other.x_;
    y_ -= other.y_;
}
void geomdraw::d2::MulD2(const d2& other) {
    x_ *= other.x_;
    y_ *= other.y_;
}
void geomdraw::d2::DivD2(const d2& other) {
    if (other.x_ == 0 || other.y_ == 0) {
        throw std::logic_error("Can't divided by zero!");
    }
    x_ /= other.x_;
    y_ /= other.y_;
}
geomdraw::d2 geomdraw::d2::operator+(const geomdraw::d2& other) const {
    d2 result(*this);
    result.SumD2(other);
    return result;
}
geomdraw::d2 geomdraw::d2::operator-(const geomdraw::d2& other) const {
    d2 result(*this);
    result.SubD2(other);
    return result;
}
geomdraw::d2 geomdraw::d2::operator*(const geomdraw::d2& other) const {
    d2 result(*this);
    result.MulD2(other);
    return result;
}
geomdraw::d2 geomdraw::d2::operator/(const geomdraw::d2& other) const {
    d2 result(*this);
    result.DivD2(other);
    return result;
}

