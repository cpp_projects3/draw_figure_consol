#CC = gcc -lstdc++
SRC_SH = shapes/figure.cpp shapes/circle.cpp
SRC_DIM = dimensions/dimension2.cpp dimensions/dimension3.cpp
#FLAGS= -Wall -Werror -Wextra -std=c++17
#TESTFLAGS=-lgtest

# -c (create): Компиляция файла в файл формата file.o . Чтобы скомпилировать отдельный файл без исполняемого \
ar -rcs: \
	-r(remove): Удалит все файлы если они походят на название создаваемого файла \
	-s(sort): Создает отсортированный индекс библиотеки.a Так будем быстрее получать доступ к функциям \
	ar (archiver): Архиватор. Собирает все объектные_файлы.o в библиотеку.a (прдеставляет из себя просто архив)

# lcov -t "s21_matrix_oop" -o s21_matrix_oop.info -c -d ./ \
	-t <имя>: устанавливаем имя отчета \
	-c (--capture): указывает, что lcov должен использовать существующие данные о coverage \
	-d <путь>: устанавливает каталог, в котором надо искать данные о coverage \
	--no-external: Не будет покаывать покрытие по испортированным стандартным библиотекам

all:
	g++ main.cpp $(SRC_SH) $(SRC_DIM) -o draw.out
clean:
	rm *.out

.PHONY: clean

