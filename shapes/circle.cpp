#include "circle.h"

geomdraw::circle::circle(int width, int height): figure(width, height) {}

void geomdraw::circle::DrawFigure(const float& shift) {
    system("clear");
    for (int i = 0; i < (width_ + 1); ++i) {
        for (int j = 0; j < height_; ++j) {
            float diff = 1;
            float x = (float) i / (float) width_ * 2.0f - 1.0f;
            float y = (float) j / (float) height_ * 2.0f - 1.0f;
            x *= coef_side_consl_ * kPixelCoefDog;
            x += sin(shift * 0.01f);
            char pixel = ' ';
            float dist = sqrt(x * x + y * y);
            int color = (int)(1.0f / dist);
            if (i == width_) {
                pixel = '\n';
            } else {
                if (color < 0) {
                    color = 0;
                }
                else if (color > gradientSize) {
                    color = gradientSize;
                }
                pixel = buf[color];
            }
            screen_[i + j * (width_ + 1)] = pixel;
        }
    }
    screen_[height_ * (width_+1)] = '\0';
    std::cout << screen_;
}