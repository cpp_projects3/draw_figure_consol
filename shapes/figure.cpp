#include "figure.h"

geomdraw::figure::figure(int width, int height): width_(width), height_(height) {
    coef_side_consl_ = (float)width_ / (float)height_;
    screen_ = new char[(width_+1) * height_];
}
geomdraw::figure::~figure() {
    if(screen_ != nullptr) {
        RemoveFigure();
    }
}

void geomdraw::figure::RemoveFigure() {
    delete[] screen_;
}