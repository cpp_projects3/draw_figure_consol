#ifndef GEOMETRY_SHAPES_H_
#define GEOMETRY_SHAPES_H_

#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include "../dimensions/dimension2.h"
#include "../dimensions/dimension3.h"

namespace geomdraw {
class figure {
protected:
    int width_;
    int height_;
    float coef_side_consl_;
    const float kPixelCoefDog = 11.0f / 24.0f;
    char* screen_;
    std::string buf = " .!/r1ZH9W$@";
    int gradientSize = (int)buf.length() - 2;

    void RemoveFigure();
public:
    figure(int width, int height);
    virtual ~figure();
    virtual void DrawFigure(const float& shift) = 0;
};
} // namespace geomdraw
#endif //GEOMETRY_SHAPES_H_
