#ifndef GEOMETRY_SQUARE_H
#define GEOMETRY_SQUARE_H

#include "figure.h"

namespace geomdraw {
class square: public figure {
public:
    square(int width, int height);
    void DrawFigure(const float& shift) override;
};
} // namespace geomdraw
#endif //GEOMETRY_CIRCLE_H

#endif //GEOMETRY_SQUARE_H
