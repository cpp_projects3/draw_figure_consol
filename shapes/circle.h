#ifndef GEOMETRY_CIRCLE_H
#define GEOMETRY_CIRCLE_H

#include "figure.h"

namespace geomdraw {
class circle: public figure {
public:
    circle(int width, int height);
    void DrawFigure(const float& shift) override;
};
} // namespace geomdraw
#endif //GEOMETRY_CIRCLE_H
